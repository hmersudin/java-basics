package com.amigoscode.exceptions;

/**
 * We can define our own Exception
 */
public class MyException extends RuntimeException {
    public MyException(String message) {
        super(message);
    }

}
