package com.amigoscode.javaessentials;

import com.amigoscode.javaessentials.classes.*;
import com.amigoscode.javaessentials.enums.SamsungModel;
import com.amigoscode.javaessentials.interfaces.AnimalInterface;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Encapsulation encapsulation = new Encapsulation(
                SamsungModel.FE_S20,
                499,
                true
        );
        System.out.println(encapsulation.getPrice());

//        Animal animal = new Animal("Lessi");
        Animal dog = new Dog("Fly", "Bullpit");
        Animal cat = new Cat("Mini", "Chartreux");

        // example of using polymorphism
        dog.makeSound();
        cat.makeSound();

        Abstraction example = new ExtendsAbstractionClass("Test name");
        example.abstractMethod();

        // polymorphism
        Polymorphism polymorphism = new Polymorphism();
        polymorphism.getPolymorphismInAction();

        // interface
        AnimalInterface dogi = new DogUsingInterface("Lesko");
        AnimalInterface cati = new CatUsingInterface("Minis");

        dogi.makeSound();
        cati.makeSound();

        // method overloading (polymorphism achieved)
        MethodOverlaoding methodOverlaoding = new MethodOverlaoding();
        System.out.println(methodOverlaoding.add(10, 5));
        System.out.println(methodOverlaoding.add(10, 5, 5));
    }
}