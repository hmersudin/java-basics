package com.amigoscode.javaessentials.classes;

/**
 * Sets cannot have duplicates and order is not guaranteed.
 */

import java.util.*;

public class GenericSets {
    public static void main(String[] args) {
        /**
         * Order is not preserved
         */
        Set<String> names = new HashSet<>();
        names.add("Sina");
        names.add("Haldun");
        names.add("Belkisa");
        names.add("Sina");
        names.add("Belkisa");

        System.out.println(names.size());
        System.out.println(names);

        /**
         * Order is preserved = new LinkedHashSet<>();
         */
        Set<String> orderNames = new LinkedHashSet<>();
        orderNames.add("Sina");
        orderNames.add("Belkisa");
        orderNames.add("Haldun");
        orderNames.add("Sina");

        System.out.println(orderNames);

        /**
         * Sorted set
         */
        Set<String> sortedNames = new TreeSet<>();
        sortedNames.add("Sina");
        sortedNames.add("Belkisa");
        sortedNames.add("Sina");
        sortedNames.add("Haldun");

        System.out.println(sortedNames);

        // loop using iterator
        Iterator<String> iterator = sortedNames.iterator();

        while (iterator.hasNext()) {
            String name = iterator.next();
            System.out.println(name);
        }
    }
}
