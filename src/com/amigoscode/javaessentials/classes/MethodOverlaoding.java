package com.amigoscode.javaessentials.classes;

/**
 * We can achieve polymorphism as well using method overloading
 */
public class MethodOverlaoding {
    public int add(int a, int b) {
        return a + b;
    }
    public int add(int a, int b, int c) {
        return a + b + c;
    }

}
