package com.amigoscode.javaessentials.classes;

public class LearnNullValue {
    public static void main(String[] args) {
        String brand = null;

        if (brand != null) {
            System.out.println(brand.toUpperCase());
        }
        else {
            System.out.println("Variable has not been set!");
        }

        /**
         * This is somehow expensive operation! If possible do not use try-catch with NPE.
         */
        try {
            System.out.println(brand.toUpperCase());
        } catch (NullPointerException e) {
            System.out.println("Brand is null!");
        }
    }
}
