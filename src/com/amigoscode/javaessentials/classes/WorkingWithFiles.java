package com.amigoscode.javaessentials.classes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Working with files
 */
public class WorkingWithFiles {
    public static void main(String[] args) {
        /**
         * Writing to file (creating file)
         */
        String fileName = "src/com/amigoscode/files/new-file.txt";
        File file = new File(fileName);
        if (!file.exists())
            try {
                file.createNewFile();
                System.out.println(file.getName() + " created!");
            } catch (IOException e) {
                System.out.println("Error creating file " + e);
            }

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println("id,name,email");
            printWriter.println("1,dino,mhasanovic@gmail.com");
            printWriter.println("2,ahmed,ahmed@gmail.com");
            printWriter.println("3,khanid,hkda@gmail.com");
            printWriter.close();
            printWriter.flush();
            System.out.println("Content added to a file!");
        } catch (IOException e) {
            System.out.println("Error writing to file! " + e);
        }

        /**
         * Reading from a file
         *
         * Parse the content of a file and turn it to an object
         */
        try {
            // reader where we pass our file "file"
            FileReader fileReader = new FileReader(file);
            // read from a file
            BufferedReader reader = new BufferedReader(fileReader);

            String line = "";
            System.out.println("Header = " + reader.readLine());

            List<User> users = new ArrayList<>();
            while((line = reader.readLine()) != null) {
                String[] split = line.split(",");
                Integer id = Integer.parseInt(split[0]);
                String name = split[1];
                String email = split[2];
                User user = new User(
                        id,
                        name,
                        email
                );
                users.add(user);
            }
            reader.close();
            users.forEach(System.out::println);

        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
