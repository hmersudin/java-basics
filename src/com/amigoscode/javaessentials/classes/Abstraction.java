package com.amigoscode.javaessentials.classes;

/**
 * abstraction -> process of hiding certain details/information from our users
 * This can be done by using abstract classes or interfaces.
 *
 *
 * Use abstract -> when you have some common code that should be used across all subclasses
 *              -> you can only extends one class
 *                  * public class Cat extends Animal {}
 */
public abstract class Abstraction {
    private String name;

    /**
     * abstract class can have:
     * 1. normal methods
     * 2. abstract methods
     */

    public Abstraction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * This is an abstract method that has to be implemented by anyone who extends this class.
     */
    public abstract void abstractMethod();

    ;
}
