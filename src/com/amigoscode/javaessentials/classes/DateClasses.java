package com.amigoscode.javaessentials.classes;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class DateClasses {
    public static void main(String[] args) {
        ZonedDateTime nowZDT = ZonedDateTime.now();
        LocalDateTime nowLDT = LocalDateTime.now();
        LocalDate nowLD = LocalDate.now();
        LocalTime nowLT = LocalTime.now();

        Instant instant = Instant.now();

        System.out.println(nowZDT);
        System.out.println(nowLDT);
        System.out.println(nowLD);
        System.out.println(nowLT);

        System.out.println(instant);

        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("Australia/West"));
        System.out.println(localDateTime);

        LocalDate today = LocalDate.of(2022, Month.SEPTEMBER, 12);
        String dateFormat = today.format(DateTimeFormatter.ofPattern("dd-MMM-YYYY"));
        System.out.println(dateFormat);
    }
}
