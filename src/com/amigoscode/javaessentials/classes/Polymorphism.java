package com.amigoscode.javaessentials.classes;

/**
 * Polymorphism -> many forms [poly => many , morphism => forms]
 *
 */
public class Polymorphism {

    public Polymorphism() {}

    /**
     * In this case '+' has many form.
     * 1. When using with strings, it concatenates two strings.
     * 2. When using with integers, it adds two numbers.
     */
    public void getPolymorphismInAction() {
        System.out.println("A" + "B");
        System.out.println(1 + 2);
    }
}
