package com.amigoscode.javaessentials.classes;

import java.util.HashMap;
import java.util.Map;

/**
 * Maps -> (key, value) pairs
 */
public class GenericMaps {
    public static void main(String[] args) {
        // save the key and its value
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Hannah");
        map.put(2, "Jamil");
        map.put(3, "Alice");

        System.out.println(map);

        // ket all values
        System.out.println(map.values());
        for (String value : map.values()) {
            System.out.println(value);
        }

        // get all keys
        System.out.println(map.keySet());
        for (int key : map.keySet()) {
            System.out.println(map.get(key));
        }

        map.forEach((key, value) -> System.out.println(key + " - " + value));

        // getOrDefault
        String defaultValueKeyNotPresent = map.getOrDefault(10, "Default Value");
        System.out.println(defaultValueKeyNotPresent);

        String defaultValueKeyPresent = map.getOrDefault(3, "Default Value");
        System.out.println(defaultValueKeyPresent);
    }
}
