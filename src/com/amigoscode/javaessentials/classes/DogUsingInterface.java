package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.interfaces.AnimalInterface;

/**
 * When using interfaces we "implements Interface"
 */
public class DogUsingInterface implements AnimalInterface {
    private String name;

    public DogUsingInterface(String name) {
        this.name = name;
    }

    @Override
    public void makeSound() {
        System.out.println("Woof, woof using interface!");
    }

    @Override
    public String getNameInterface() {
        return this.name;
    }
}
