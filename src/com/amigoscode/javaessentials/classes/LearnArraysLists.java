package com.amigoscode.javaessentials.classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Data Structure:
 *  1. Arrays
 *  2. Lists
 */
public class LearnArraysLists {
    public static void main(String[] args) {
        // Arrays
        int[] numbersA = new int[3];
        numbersA[1] = 10;
        numbersA[2] = 20;
        numbersA[0] = 30;

        int[] numbersB = {1, 2, 3};
        System.out.println(Arrays.toString(numbersB));

        for (int number : numbersA) {
            System.out.println(number);
        }

        for (int i = 0; i < numbersB.length; i++) {
            System.out.print(i + " ");
        }
        System.out.println("---");

        // Lists
        List numbers = new ArrayList();
        numbers.add(100);
        numbers.add(200);
        numbers.add(300);
        numbers.add("A");
        numbers.add("B");

        System.out.println(numbers);

        numbers.forEach(System.out::println);

        for (Object number : numbers) {
            System.out.println(number);
        }

        // methods with List
        numbers.remove(2);
        System.out.println(numbers.contains(900));
        System.out.println(numbers.isEmpty());
        System.out.println(numbers.size());

        numbers.forEach(System.out::println);

//        numbers.clear();
        System.out.println("After clearing (deleting all elements)!");
        numbers.forEach(System.out::println);


    }
}
