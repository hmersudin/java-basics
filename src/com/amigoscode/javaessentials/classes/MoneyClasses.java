package com.amigoscode.javaessentials.classes;

import java.math.BigDecimal;

public class MoneyClasses {
    public static void main(String[] args) {
        double numberOne = 0.02;
        double numberTwo = 0.03;
        double result = numberTwo - numberOne;

        System.out.println(result);

        // Big decimals
        BigDecimal numOne = new BigDecimal("0.02");
        BigDecimal numTwo = new BigDecimal("0.03");
        BigDecimal res = numTwo.subtract(numOne);

        System.out.println(res);
        System.out.println(numTwo.compareTo(numOne));

        double a = 1;
        double b = 2;

        System.out.println(a <= b);
        System.out.println(a >= b);

    }
}
