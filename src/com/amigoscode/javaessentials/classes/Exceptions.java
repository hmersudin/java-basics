package com.amigoscode.javaessentials.classes;

public class Exceptions {
    public static void main(String[] args) {
//        String brand = "Amigoscode";
        String brand = null;

        try {
            System.out.println(brand.length());
        } catch (Exception e) {
            System.out.println("Brand was null! " + e);
        }
    }
}
