package com.amigoscode.javaessentials.classes;

public class Cat extends Animal{
    private String breed;

    public Cat(String name, String breed) {
        super(name);
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    /**
     * Implementation of the abstract method from abstract Animal class
     */
    @Override
    public void makeSound() {
        System.out.println("Miau, miau!");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + super.getName() + '\'' + " " +
                "breed='" + breed + '\'' +
                '}';
    }
}
