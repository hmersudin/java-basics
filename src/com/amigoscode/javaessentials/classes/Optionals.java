package com.amigoscode.javaessentials.classes;

import java.util.Optional;

/**
 * Optional => preferred way of dealing with NPE.
 */
public class Optionals {
    public static void main(String[] args) {
        String str = null;

        // You know => optional is empty
        Optional.empty();
        // You know => value will never be null!
        Optional.of("Not Null optional");
        // You are now sure => weather if it's null
        Optional.ofNullable(str);

//        String brand = "Amigoscode";
        String brand = null;
        /**
         * We are not sure if 'brand' is null, so we use 'Optional.ofNullable(brand)'
         * In case it's a null, then set the default values for 'brand'
         * 'brand' => "Default value when 'brand' is null"
         */
        String theBrand = Optional.ofNullable(brand)
                .orElse("Default value when 'brand' is null!");
        System.out.println(theBrand);

        /**
         * It passes lambda and some logic, when using .orElseGet(...)
         * () -> { //logic }
         */
        String theBrands = Optional.ofNullable(brand)
                        .orElseGet(() -> {
                            // have some logic inside and then simple return a default value
                            return "Default value 1!";
                        });
        System.out.println(theBrands);

        /**
         * We are not sure if 'brand' is null, so we use 'Optional.ofNullable(brand)'.
         * Furthermore, we can apply some methods on it, e.g. map it to upperCase, then if null
         * we can apply some logic and then return some defined default value!
         *
         */
        String brands = Optional.ofNullable(brand)
                .map(String::toUpperCase)
                .orElseGet(() -> {
                    // logic
                    return "Default value 2!";
                });
        System.out.println(brands);
    }
}
