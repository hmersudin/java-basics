package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.enums.Gender;
import com.amigoscode.javaessentials.enums.SamsungModel;

/**
 * Class -> blueprint for creating multiple objects (anything you want).
 * Created object is a real thing.
 */
public class Classes {
    public static void main(String[] args) {
        /**
         * note8  -> instance of the Samsung class
         * using default constructor
         */
        Samsung note8 = new Samsung();
        note8.setModel(SamsungModel.GALAXY_NOTE8);
        note8.setPrice(299.99);
        note8.setWaterResistant(true);

        System.out.println(note8);

        /**
         * s23 -> another instance of the Samsung class
         * using constructor
         */
        Samsung s23 = new Samsung(
                SamsungModel.GALAXY_S23,
                1299.99,
                true
        );

        System.out.println(s23);

        Samsung fe20 = new Samsung(
                SamsungModel.GALAXY_S23,
                499.99
        );

        // toString()
        System.out.println(fe20.getModel().getModelStr());

        /**
         * Use the Student class
         */

        Student student = new Student(
                "Ali",
                23,
                Gender.MALE
        );

        System.out.println(student);
    }
}


