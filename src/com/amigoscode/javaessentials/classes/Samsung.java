package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.enums.SamsungModel;

/**
 * private => hide details from outside world aka. encapsulation.
 * this -> current instance of the current class
 */
public class Samsung {
    // properties
    private SamsungModel model;
    private double price;
    private boolean isWaterResistant;

    // constructors
    public Samsung() {}

    public Samsung(SamsungModel model, double price, boolean isWaterResistant) {
        this.model = model;
        this.price = price;
        this.isWaterResistant = isWaterResistant;
    }

    // constructor with a default values
    public Samsung(SamsungModel model, double price) {
        // we use 'this' to call other constructors
        this(model, price, false);

        this.model = model;
        this.price = price;
    }

    //behaviour (functionality of the Samsung phone)

    // we can replace this method with toString()
    public void printDetails() {
        System.out.println("Model: " + this.model);
        System.out.println("Price: " + this.price);
        System.out.println("Water resistant: " + this.isWaterResistant);
    }

    public SamsungModel getModel() {
        return model;
    }

    //this -> current instance of the current class
    public void setModel(SamsungModel model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isWaterResistant() {
        return isWaterResistant;
    }

    public void setWaterResistant(boolean waterResistant) {
        this.isWaterResistant = waterResistant;
    }

    @Override
    public String toString() {
        return "Samsung{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", isWaterResistant=" + isWaterResistant +
                '}';
    }
}
