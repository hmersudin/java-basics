package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.interfaces.AnimalInterface;

/**
 * When using interfaces we "implements Interface"
 */
public class CatUsingInterface implements AnimalInterface {
    private String name;

    public CatUsingInterface(String name) {
        this.name = name;
    }

    @Override
    public void makeSound() {
        System.out.println("Miaow, miaow using interface!");
    }

    @Override
    public String getNameInterface() {
        return this.name;
    }
}
