package com.amigoscode.javaessentials.classes;

import com.amigoscode.exceptions.MyException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * There are two types of exceptions:
 * 1. Checked Exception
 * need to be handled at compile time
 * (handling it using:
 * -> try{} catch{} block or
 * -> using throws keyword)
 * otherwise it will give us compilation error
 * <p>
 * 2. Unchecked Exception (not checked compile time)
 * -> if you do not handle it, program will not give you compilation error
 */
public class TypesOfExceptions {
    public static void main(String[] args) throws FileNotFoundException {
        // Checked exceptions

        // using throws FileNotFoundException
        FileInputStream inputStreamUsingThrows = new FileInputStream("src/com/amigoscode/files/file.txt");

        // using try{} catch{} block
        try {
            FileInputStream inputStreamUsingTryCatch = new FileInputStream("src/com/amigoscode/files/file.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Unchecked exceptions (no checking at the compile time)
        try {
            var result = 10 / 0;
            System.out.println(result);
        } catch (ArithmeticException | NullPointerException | NumberFormatException e) {
            System.out.println("Cannot divide number by 0! " + e);
        } finally {
            System.out.println("cleaning up... (always executed)");
        }

        /**
         * Different keyword to use exceptions
         * 1. try {} catch {}
         * 2. finally (always executed - cleaning purposes)
         * 3. using multiple exception types
         * 4. throw new MyException (creating our own exception handling)
         */
        System.out.println(performDivision(1, 1));
    }

    static double performDivision(double a, double b) {
        if (b == 0) {
            throw new MyException("Cannot divide by 0!");
        }
        return a / b;
    }
}
