package com.amigoscode.javaessentials.classes;

public class ExtendsAbstractionClass extends Abstraction{
    public ExtendsAbstractionClass(String name) {
        super(name);
    }

    /**
     * Implementation of the abstract method extended from the abstract class Abstraction
     */
    @Override
    public void abstractMethod() {
        System.out.println("This is implementation of the extended abstract class Abstraction within " + this.getClass());
    }
}
