package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.enums.Gender;

import java.util.ArrayList;
import java.util.List;

public class GenericList {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);

        // print list of numbers
        System.out.println(numbers);

        // normal print format
        for (Integer number : numbers) {
            System.out.println(number);
        }

        // normal print format
        numbers.forEach(System.out::println);

        List<String> cities = new ArrayList<>();
        cities.add("SBG");
        cities.add("DMM");
        cities.add("SJP");

        System.out.println(cities);

        List<Student> students = new ArrayList<>();
        students.add(new Student("Ibn Sina", 89, Gender.MALE));
        students.add(new Student("Belkisa", 45, Gender.FEMALE));
        students.add(new Student("Haldun", 63, Gender.MALE));

        students.forEach(System.out::println);

        List<Animal> animals = new ArrayList<>();
        animals.add(new Dog("Brendy", "Pitbull"));
        animals.add(new Cat("Lisy", "Chartreux"));

        System.out.println(animals);
    }
}
