package com.amigoscode.javaessentials.classes;

import com.amigoscode.javaessentials.enums.SamsungModel;

/**
 * Encapsulation -> data from your class should be hidden from the outside  world
 * and only be accessible through our methods.
 * It means all of our data within the class have to be private
 *
 * private -> accessible only within that class
 */
public class Encapsulation {
    private SamsungModel model;
    private double price;
    private boolean isAvailable;

    public Encapsulation() {
    }

    public Encapsulation(SamsungModel model, double price, boolean isAvailable) {
        this.model = model;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public SamsungModel getModel() {
        return model;
    }

    public void setModel(SamsungModel model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be less than zero!");
        }
        else
            this.price = price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
