package com.amigoscode.javaessentials.classes;

/**
 * When using abstract/inheritance we use "extends".
 */
public class Dog extends Animal {
    private String breed;

    /**
     * Create a constructor with a super
     * using the name of the parent class
     *
     * @param name
     */
    public Dog(String name, String breed) {
        super(name);
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + super.getName() + '\'' + " " +
                "breed='" + breed + '\'' +
                '}';
    }

    /**
     * Implementation of the abstract method from abstract Animal class
     */
    @Override
    public void makeSound() {
        System.out.println("Woof, woof!");
    }
}
