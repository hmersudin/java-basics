package com.amigoscode.javaessentials.classes;

import com.amigoscode.javabasics.Person;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class LearnStackQueues {
    public static void main(String[] args) {
        Stack<Integer> integerStack = new Stack<>();
        integerStack.push(1);
        integerStack.push(2);
        integerStack.push(3);
        integerStack.push(4);

        System.out.println(integerStack.peek());
        integerStack.pop();

        // Last In First Out (LIFO)
        System.out.println(integerStack);

        // First In First Out (FIFO)
        Queue<Person> stringQueue = new LinkedList<>();
        stringQueue.add(new Person("James", "Bond", 28));
        stringQueue.add(new Person("Sara", "Malkon", 24));

        System.out.println(stringQueue.peek());

        System.out.println(stringQueue);

        System.out.println("for");
        stringQueue.forEach(person -> System.out.println(person));
    }
}

