package com.amigoscode.javaessentials.enums;

public enum Gender {
    MALE,
    FEMALE
}
