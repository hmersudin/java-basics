package com.amigoscode.javaessentials.enums;

public enum SamsungModel {
    GALAXY_S23("Samsung Galaxy S23"),
    FE_S20("Samsung Galaxy FE20"),
    GALAXY_NOTE8("Samsung Galaxy Note8");

    private String modelstr;

    SamsungModel(String model) {
        this.modelstr = model;
    }

    public String getModelStr() {
        return modelstr;
    }
}
