package com.amigoscode.javaessentials.interfaces;

/**
 * interface -> similar to abstract, with slightly differences.
 */
public interface AnimalInterface {
    /**
     * With interfaces, we are not allowed:
     * 1. to have properties!
     * 2. to have implementation of the methods.
     *
     * we are allowed to have only:
     * 1. abstract methods
     */

    void makeSound();
    String getNameInterface();

}
