package com.amigoscode.javabasics;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LearnDataTypes {
    public static void main(String[] args) {
        // Primitive data types
        /**
         * Stores whole numbers
         * use 2_442_242 for readability
         */
        byte theByte = 127; // -128 to 127
        short theShort = 2_929; // 32768 to 32767
        int theInt = 2_823_883;
        long theLong = 23_232_823_823_823L; // add L or l
        /**
         * Stores decimal numbers
         */
        float pi = 3.14F;
        double doublePi = 3.1415;
        /**
         * Stores boolean values
         */
        boolean isAdult = true;
        /**
         * Stores single character
         */
        char nameInitial = 'J';

        // e.g.
        int a = 10;
        int b = a;
        a = 100;
        System.out.println("a: " + a);
        System.out.println("b: " + b);

        // Reference types
        String courseName_obj = new String("Java Basics");
        String courseName = "Java Basics";
        if (courseName.equals(courseName_obj)) {
            System.out.println(courseName.toUpperCase());
        }
        System.out.println(courseName);
        LocalDate now = LocalDate.now();

        // logical operations
        boolean TRUE = true;
        boolean FALSE = false;

        /**
         * AND && operator
         */
        System.out.println(TRUE && TRUE);
        System.out.println(TRUE && FALSE);
        System.out.println(FALSE && FALSE);
        /**
         * OR || operator
         */
        System.out.println(TRUE || TRUE);
        System.out.println(TRUE || FALSE);
        System.out.println(FALSE || FALSE);
        /**
         * ! NOT operator
         */
        System.out.println(TRUE && !FALSE);

        // if statement
        int age = 11;
        if (age >= 18) {
            System.out.println("You are an adult");
        } else System.out.println("You are not an adult");

        System.out.println(age >= 18 ? "Adult" : "Not adult");

        String gender = "male";
        switch (gender.toUpperCase()) {
            case "MALE":
                System.out.println("Male group");
                break;
            case "FEMALE":
                System.out.println("Female group");
                break;
            default:
                System.out.println("Prefer not to say!");
        }
    }
}
