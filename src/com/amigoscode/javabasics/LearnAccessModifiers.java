package com.amigoscode.javabasics;

public class LearnAccessModifiers {

    /**
     * public -> access modifier
     * static -> means it belongs to a class
     * void -> return type
     * main -> name of the method
     * String[] args -> arguments
     */
    public static void main(String[] args) {

    }

    /**
     * Phone static --> package protected.
     * Cannot be access from other packages.
     * Only accessible from within this package.
     */
    static class Phone {}

    /**
     * public --> Tablet class accessible from everywhere
     */
    public class Tablet {}

    /***
     * private --> TV class accessible only from within this class
     */
    private class TV {}
}
