package com.amigoscode.javabasics;

import java.util.ArrayList;
import java.util.List;

/**
 * static --> it belongs to a class
 */
public class LearnStatic {
    public static String BRAND;
    static List list;

    static {
        BRAND = "Amigoscode";
        list = new ArrayList();
        list.add(1);
    }

    public static void main(String[] args) {
        Student alex = new Student("Alex");
        Student mary = new Student("Mary");

        System.out.println();
        System.out.println(alex.name);
        System.out.println(mary.name);
        System.out.println(Student.createdStudent);

    }

    static class Student {
        String name;
        static int createdStudent = 0;

        public Student(String name) {
            this.name = name;
            createdStudent++;
        }
    }
}
