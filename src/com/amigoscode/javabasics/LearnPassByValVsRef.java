package com.amigoscode.javabasics;

import java.awt.*;

public class LearnPassByValVsRef {
    public static void main(String[] args) {
        int a = 0;
        incrementValueWrong(a);
        int x = incrementValue(a);
        System.out.println(a);
        System.out.println(x);

        Point point = new Point(100, 100);
        changePoint(point);
        System.out.println(point);
    }

    /**
     * When you pass primitive data types as an argument within a function,
     * it is passed by value (copy of actual value).
     * Never mutate primitive values.
     *
     * @param value
     */
    static void incrementValueWrong(int value) {
        value++;
    }

    /**
     * Use it like this. Return a new one.
     * @param value
     * @return
     */
    static int incrementValue(int value) {
        return value + 1;
    }

    /**
     * Object (Reference) types are passed by reference (original value).
     * We are mutating original object.
     *
     * @param point
     */
    static void changePoint(Point point) {
        point.x = 0;
        point.y = 0;
    }
}
