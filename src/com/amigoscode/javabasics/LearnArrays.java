package com.amigoscode.javabasics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Arrays
 */
public class LearnArrays {
    public static void main(String[] args) {
        System.out.println("---arrays---");

        int[] numbers = new int[3];
        System.out.println(Arrays.toString(numbers));
        numbers[0] = 0;
        numbers[1] = 1;
        numbers[2] = 3;
        System.out.println(Arrays.toString(numbers));

        int[] ids = {1, 2, 3, 5};
        System.out.println(Arrays.toString(ids));

        String[] names = {"Zak", "Ali", "Maria"};
        System.out.println(Arrays.toString(names));

        System.out.println("Total: " + Arrays.stream(names).count());
        System.out.println("Sorted: " +
                Arrays.stream(names)
                        .sorted()
                        .collect(Collectors.toList()));
        System.out.println("Total length: " + names.length);

        /**
         * Printing the elements of the arrays
         */
        System.out.println("---printing elements of arrays using for loops---");
        // for loops
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        /**
         * Enhanced for loop
         *
         * From numbers take the individual numbers and put it inside number
         */
        System.out.println("Enhanced for loop");
        // for (dataType variable : array
        for (int number : numbers) {
            System.out.println(number);
        }

        List<String> cities = new ArrayList<>();
        cities.add("Salzburg");
        cities.add("Wien");
        cities.add("St.Johann im Pongau");

        for (String city : cities) {
            System.out.println(city);
        }
        // forEach
        cities.forEach(System.out::println);

        // bonus tip
        /**
         * cities.fori
         */
        for (int i = 0; i < cities.size(); i++) {

        }
        /**
         * cities.forr
         */
        for (int i = cities.size() - 1; i >= 0; i--) {

        }
        /**
         * cities.for
         */
        for (String city : cities) {

        }
        /**
         * cities.forEach
         */
        cities.forEach(System.out::println);

        System.out.println("---break, continue---");
        for (int number : numbers) {
            if (number != 0 && number != 1)
                // exit the loop
                break;
            System.out.println(number);
        }

        for (String city : cities) {
            if (city.startsWith("W"))
                //goes back to the beginning of the loop
                continue;
            System.out.println(city);
        }
        // do while
        int count = 1;

        do {
            System.out.print(count + "");
            count++;
        }
        while (count <= 5);

        System.out.println();
        // while loop
        count = 1;
        while (count <= 5) {
            System.out.print(count + " ");
            count++;
        }


    }
}