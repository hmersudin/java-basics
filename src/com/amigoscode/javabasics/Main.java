package com.amigoscode.javabasics;
/**
 * Java Basics - online course from amigoscode.com platform.
 * Starting point of learning Java programming language.
 *
 * @author mhasanovic
 */
public class Main {
    public static void main(String[] args) {

        Person person = new Person("Zak", "Bel", 24);
        person.setFirstName("Ali");
        person.setLastName("Lathia");
        person.setAge(26);

        System.out.println(person);
    }
}