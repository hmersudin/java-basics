package com.amigoscode.javabasics;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class LearnMethods {
    public static void main(String[] args) {
        printDate();

        char[] letters = {'A', 'C', 'B', 'C', 'C', 'C', 'B', 'A', 'C', 'B'};
        countOccurences(letters);
        System.out.println("Total " + countOccurencesOfLetter(letters, 'C'));

        var currentDate = getCurrentDate();
        System.out.println(currentDate);
    }

    public static void countOccurences(char[] letters) {
        int countA = 0, countB = 0, countC = 0;
        for (char letter : letters) {
            if (letter == 'A')
                countA++;
            else if (letter == 'B')
                countB++;
            else countC++;
        }
        System.out.println("Total A: " + countA);
        System.out.println("Total B: " + countB);
        System.out.println("Total C: " + countC);
    }

    public static int countOccurencesOfLetter(char[] letters, char searchLetter) {
        System.out.println(Arrays.toString(letters));
        System.out.println("Searched letter: " + searchLetter);
        int count = 0;
        for (char letter : letters) {
            if (searchLetter == letter) {
                count++;
            }
        }
        return count;
    }

    public static void printDate() {
        LocalDate today = LocalDate.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MMM.yyyy");
        System.out.println(today.format(dateTimeFormatter));
    }

    private static LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
