package com.amigoscode.javabasics;

import java.time.LocalDate;
import java.util.Scanner;

public class LearnScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name: ");
        String username = scanner.nextLine();

        System.out.println("Enter your birth year: ");
        int birthYear = scanner.nextInt();

        LocalDate now = LocalDate.now();
        int age = now.getYear() - birthYear;

        System.out.println("User " + username + " is " + age + " years old!");
    }
}
