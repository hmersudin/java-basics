package com.amigoscode.javabasics;

import java.awt.*;
import java.util.Arrays;

public class VarFinalKeyword {
    public static void main(String[] args) {
        var name = "Jamila";
        var ids = 123;
        var names = new String[]{"Abc", "Dec"};
        System.out.println(name);
        System.out.println(ids);
        System.out.println(Arrays.toString(names));


        final double PI = 3.14;
        final var DOUBLE_PI = PI * 2;
        System.out.println(PI);
        System.out.println(DOUBLE_PI);

        /**
         * With reference types we can change the actual values, but we cannot
         * reassign it again.
         * final Point point = new Point(1, 2):
         * point = new Point(10, 20);   --> not allowed
         */
        final Point point = new Point(1,2);
        point.x = 10;
        point.y = 20;

        System.out.println(point);
    }


}
