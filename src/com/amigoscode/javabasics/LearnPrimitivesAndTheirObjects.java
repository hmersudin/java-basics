package com.amigoscode.javabasics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * For every primitive we have Object type
 * int -> Integer
 * double -> Double
 * long -> Long
 * etc.
 */
public class LearnPrimitivesAndTheirObjects {
    public static void main(String[] args) {
        int num = 1;
        Integer number = 1;
        double price = 2.99;
        Double priced = 3.99;

        var shortValue= number.shortValue();
        System.out.println(shortValue);

        List<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(11);
        numbers.add(12);

        System.out.println(Arrays.toString(numbers.toArray()));

        for (Integer integer : numbers) {
            System.out.println(integer);
        }

        numbers.forEach(System.out::println);
    }
}
