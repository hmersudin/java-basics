package com.amigoscode.javabasics;

import java.util.stream.Collectors;

/**
 * Object class -> root of every other classes
 */
public class LearnObjectClass {
    public static void main(String[] args) {
        Object name = "Jamil";
        Object name2 = "Jamil";

        var getObjectClass = name.getClass();
        var getObjectBytes = ((String) name).getBytes();
        System.out.println(getObjectClass);
        System.out.println(getObjectBytes);

        var getObjectHashCode = name.hashCode();
        var getObjectHashCode2 = name2.hashCode();
        System.out.println(getObjectHashCode);
        System.out.println(getObjectHashCode2);

        if (getObjectHashCode == getObjectHashCode2) {
            System.out.println("Objects are containing same content!");
        }

        var getObjectMatches = ((String) name).matches("Jamila");
        System.out.println(getObjectMatches);

        var lines = ((String) name).lines();
        System.out.println(lines.collect(Collectors.toList()));
    }
}
