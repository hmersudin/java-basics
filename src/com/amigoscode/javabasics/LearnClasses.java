package com.amigoscode.javabasics;

import com.sun.security.jgss.GSSUtil;

import java.time.LocalDate;

public class LearnClasses {
    public static void main(String[] args) {
        Lens firstLens = new Lens(
                "Sony",
                "95mm",
                true,
                599);

        Lens secondLens = new Lens(
                "Canon",
                "70mm",
                false,
                750);

        System.out.println("Lens 1");
        firstLens.printLens();

        System.out.println("Lens 2");
        secondLens.printLens();

        /**
         * Object (instance) of the blueprint class Passport
         */
        Passport passport = new Passport(
                "B1239744",
                "BiH",
                LocalDate.of(2025,11,25)
        );

        passport.printPassportDetails();

    }

    /**
     * This is a blueprint (template) for creating Lenses
     * We use static keyword to be able to use this class within the main method
     */
    static class Lens {
        String brand;
        String focalLength;
        boolean isPrime;
        double price;

        /**
         * this -> refer to the current instance of the current class
         * @param brand
         * @param focalLength
         * @param isPrime
         */
        Lens(String brand, String focalLength, boolean isPrime, double price) {
            this.brand = brand;
            this.focalLength = focalLength;
            this.isPrime = isPrime;
            this.price = price;
        }

        void printLens() {
            System.out.println("Brand: " + this.brand);
            System.out.println("Focal Length: " + this.focalLength);
            System.out.println("Prime: " + this.isPrime);
            System.out.println("Price: " + this.price);
            System.out.println("---");
        }
    }

    static class Passport {
        String number;
        String country;
        LocalDate expiryDate;

        public Passport(String number, String country, LocalDate expiryDate) {
            this.number = number;
            this.country = country;
            this.expiryDate = expiryDate;
        }

        void printPassportDetails() {
            System.out.println("Passport number: " + this.number);
            System.out.println("Country: " + this.country);
            System.out.println("Expiry Date: " + this.expiryDate);
        }
    }
}


